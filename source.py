# https://refactoring.guru/ru/design-patterns/decorator

# не забыть установить transliterate

from transliterate import translit


# decorator lowercase
def to_lowercase(function):
	def actual(message):
		return function(message.lower())
	return actual
    
# decorator spam
def spam_filter(function):
	def actual(message):
		bad_words = ['Sale', 'sale', 'сале', 'Сале']
		return function(' '.join([word for word in message.split() if word not in bad_words]))
	return actual
    
# decorator trim
def trim_filter(function):
	def actual(message):
		return function(message.replace(' ', ''))
	return actual
    
# decorator translit
def translit_filter(function):
	def actual(message):
		return function(translit(message, 'ru'))
	return actual

@translit_filter
@to_lowercase
@spam_filter
@trim_filter
def check_string(message):
	return message


# Example

line = 'Sale Simple line for Example!'

format_string = check_string(line)
print("text: %s" % format_string)